import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vendor_grigora/constants.dart';
import 'package:vendor_grigora/views/landing_widget.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: kAppName,
      theme: kThemeData,
      // darkTheme: kThemeDataDark,
      debugShowCheckedModeBanner: false,
      home: LandingWidget(),
    );
  }
}
