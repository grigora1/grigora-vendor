
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';



const String kThemeLocal = 'THEME_DATA';
const String kAppName = 'Grigora Vendor';

// Spacing
const double kPadding = 5;
const double kSmallPadding = 10;
const double kRegularPadding = 15;
const double kMediumPadding = 20;
const double kBoldPadding = 35;
const double kLargePadding = 40;
const double kWidthRatio = 0.9;
const double kIconSize = 24;
double kCalculatedWidth(Size size) => size.width * kWidthRatio;
double kCalculatedMargin(Size size) => size.width * (1 - kWidthRatio) / 2;

// Border
const double kBorderWidth = 1;
const double kThickBorderWidth = 3;
const BorderRadius kBorderRadius =
    BorderRadius.all(const Radius.circular(kSmallPadding));
const BorderRadius kFullBorderRadius =
    BorderRadius.all(const Radius.circular(100));
final BoxDecoration kTextFieldBoxDecoration = BoxDecoration(
    borderRadius: kBorderRadius,
    border: Border.all(color: kThemeData.dividerColor),
    color: Colors.white);
BoxDecoration kRoundedEdgesBoxDecoration(
        {Color backgroundColor,
        Color shadowColor,
        Color borderColor,
        double borderWidth = kThickBorderWidth,
        double radius = kSmallPadding,
        String image,
        BoxFit fit = BoxFit.cover}) =>
    BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        border: borderColor != null
            ? Border.all(color: borderColor, width: borderWidth)
            : null,
        boxShadow: shadowColor != null ? [kBoxShadow(shadowColor)] : null,
        color: backgroundColor ?? kScaffoldBackgroundColor,
        image: image != null
            ? DecorationImage(
                image: AssetImage(
                  image,
                ),
                fit: fit)
            : null);
final BoxDecoration kButtonBoxDecoration = BoxDecoration(
    borderRadius: kFullBorderRadius, border: Border.all(color: kPrimaryColor));
final BoxDecoration kBottomSheetBoxDecoration = BoxDecoration(
  color: Colors.white,
  borderRadius: new BorderRadius.only(
    topLeft: const Radius.circular(25.0),
    topRight: const Radius.circular(25.0),
  ),
);
BoxShadow kBoxShadow(Color color) => BoxShadow(
      color: color,
      spreadRadius: 0,
      blurRadius: 5,
      offset: Offset(0, 2), // changes position of shadow
    );

// Colors
const Color kPrimaryColor = Color(0xFFFFC100);
const Color kPrimaryTextColor = Color(0xFF484848);
const Color kSecondaryTextColor = Colors.white;
const Color kScaffoldBackgroundColor = Colors.white;
final Color kBorderColor = Colors.grey[350];
final Color kSubtextColor = Colors.grey[400];
final Color kColorBlack = Colors.black;
final Color kColorGrey = Colors.grey[100];
final Color kColorDarkGrey = Color(0xFF707070);

// Text
final TextStyle kHeadline1TextStyle = GoogleFonts.muli(
    textStyle: TextStyle(
        fontSize: 30, fontWeight: FontWeight.bold, color: kPrimaryTextColor));
final TextStyle kHeadline2TextStyle = GoogleFonts.muli(
    textStyle: TextStyle(
        fontSize: 25, fontWeight: FontWeight.normal, color: kPrimaryTextColor));
final TextStyle kHeadline3TextStyle = GoogleFonts.muli(
    textStyle: TextStyle(
        fontSize: 21, fontWeight: FontWeight.normal, color: kPrimaryTextColor));
final TextStyle kBodyText1Style = GoogleFonts.notoSans(
    textStyle: TextStyle(fontSize: 16, color: kPrimaryTextColor));
final TextStyle kBodyText2Style = GoogleFonts.notoSans(
    textStyle: TextStyle(fontSize: 14, color: kPrimaryTextColor));
final TextStyle kSubtitle1Style = GoogleFonts.notoSans(
    textStyle: TextStyle(fontSize: 12, color: kSubtextColor));
final TextStyle kSubtitle2Style = GoogleFonts.notoSans(
    textStyle: TextStyle(fontSize: 12, color: kPrimaryTextColor));
String kPriceFormatter(double price) =>
    '\$' + NumberFormat("#,##0.00", "en_US").format(price);
List kBuilderName(String name) => name.split(' ') ?? "";

// Theme
final ThemeData kThemeData = ThemeData.light().copyWith(
    primaryColor: kPrimaryColor,
    scaffoldBackgroundColor: kScaffoldBackgroundColor,
    dividerColor: kBorderColor,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    iconTheme: IconThemeData(size: kIconSize),
    textTheme: TextTheme(
        headline1: kHeadline1TextStyle,
        headline2: kHeadline2TextStyle,
        headline3: kHeadline3TextStyle,
        bodyText1: kBodyText1Style,
        bodyText2: kBodyText2Style,
        subtitle1: kSubtitle1Style,
        subtitle2: kSubtitle2Style));

final ThemeData kThemeDataDark = ThemeData.dark().copyWith(
    primaryColor: kPrimaryColor,
    primaryColorDark: Colors.black,
    primaryColorLight: Colors.white,
    scaffoldBackgroundColor: Colors.grey[900],
    dividerColor: kBorderColor,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    iconTheme: IconThemeData(size: kIconSize),
    textTheme: TextTheme(
        headline1: kHeadline1TextStyle.copyWith(color: Colors.white),
        headline2: kHeadline2TextStyle.copyWith(color: Colors.white),
        headline3: kHeadline3TextStyle.copyWith(color: Colors.white),
        bodyText1: kBodyText1Style.copyWith(color: Colors.white),
        bodyText2: kBodyText2Style.copyWith(color: Colors.white),
        subtitle1: kSubtitle1Style.copyWith(color: Colors.white),
        subtitle2: kSubtitle2Style.copyWith(color: Colors.white)));


extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

